import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ics-ans-role-syslog-ng-centos')


def test_syslog_ng_is_running(host):
    sysng = host.service("syslog-ng")
    assert sysng.is_running
    assert sysng.is_enabled


def test_syslog_ng(host):
    message = "ics-ans-role-syslog-ng-logger"
    result = host.run("logger -p daemon.err " + message)
    assert result.rc == 0
    syslog_messages = host.file("/var/log/messages").content_string
    assert message in syslog_messages
